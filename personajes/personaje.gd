extends KinematicBody2D

export (int) var velocidad = 200
export (float) var modif_gravedad = 0.6666
export (int) var fuerza_salto = 600
var gravedad = ProjectSettings.get("physics/2d/default_gravity")
var mov = Vector2()
var input = { 'der':0, 'izq':0, 'up':0, 'dwn':0, 'jmp':0}
export var puede_moverse = false
var saltando1 = false
var saltando2 = false
var animacion = ""
var acum = 0.0
signal saltando


# warning-ignore:unused_argument
func _physics_process(delta):
	if puede_moverse == true:
#		print("puede moverse")
		leer_entrada()
		calcular_movimiento()
		mov = move_and_slide(mov,Vector2(0,-1))
		
		acum +=delta    
		if acum>0.2:
			esteticas()
			acum=0.0
#	else:
#		print("no puede moverse")


func leer_entrada():
	input['izq'] = int(Input.is_action_pressed("ui_left"))
	input['der'] = int(Input.is_action_pressed("ui_right"))
	input['jmp'] = int(Input.is_action_just_pressed("ui_accept"))
	input['dwn'] = int(Input.is_action_just_pressed("ui_down"))

func calcular_movimiento():
	# Movimiento HORIZONTAL 
	mov.x = (input['der'] - input['izq']) * velocidad
	
	# Movimiento VERTICAL
	mov.y += gravedad * modif_gravedad
	if is_on_floor():
		saltando1 = false
		saltando2 = false
		
		if input['jmp'] != 0 :
			mov.y = -fuerza_salto
			saltando1 = true
	
	else:
		if !saltando2 and input['jmp'] !=0:
			mov.y = -fuerza_salto * 2 / 3
			saltando2 = true
			 

func esteticas():
	if mov.x>0:
		$Sprite.flip_h = false
	elif mov.x<0:
		$Sprite.flip_h = true
	
	animacion = $AnimationPlayer.get_current_animation()
	if mov.x != 0 and animacion!="run":
		$AnimationPlayer.play("run")
	elif mov.x==0 and animacion!="idle":
		$AnimationPlayer.play("idle")
	
	if saltando1 and (not saltando2) and animacion!="jump":
		$AnimationPlayer.stop()
		$AnimationPlayer.play("jump")
	elif saltando2 and animacion!="roll":
		$AnimationPlayer.stop()
		$AnimationPlayer.play("roll")

func _habilitar_movimiento():
	puede_moverse = true