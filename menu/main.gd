extends Node2D

func _on_Salir_button_up() -> void:
	get_tree().quit()

func _on_Iniciar_button_up() -> void:
	get_tree().change_scene("res://niveles/nivel1.tscn")