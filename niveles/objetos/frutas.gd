extends Node2D

var orden_frutas = Array()
var orden_posiciones = Array()
var variedades_frutas = 8
var posiciones_frutas = 16
var frutas_obtenidas = 0
signal sin_frutas

func _ready() -> void:
	calcular_orden_spawn_frutas()
	calcular_posiciones_frutas()
	colocar_frutas()



func calcular_orden_spawn_frutas():
	randomize()
	while orden_frutas.size()<variedades_frutas:
		var actual = randi() % variedades_frutas + 1
		if not orden_frutas.has(actual):
			orden_frutas.append(actual)
		print(orden_frutas)


func calcular_posiciones_frutas():
	randomize()
	while orden_posiciones.size()<variedades_frutas:
		var actual = randi() % posiciones_frutas + 1
		if not orden_posiciones.has(actual):
			orden_posiciones.append(actual)
		print(orden_posiciones)


func colocar_frutas():
	var ind_pos_actual = 0
	for i in orden_frutas:

		var f = load("res://niveles/objetos/fruta.tscn").instance()
		f.nro = orden_frutas[ind_pos_actual]
		print("numero" + str(f.nro))

		var destino = "F" + str(orden_posiciones[ind_pos_actual])
		print ("destino: " + destino)

		var posicion = self.get_node(destino)
		print (posicion.name)

		f.connect("fruta_collected",self,"_on_fruta_collected")

		posicion.add_child(f)
		ind_pos_actual+=1

func _on_fruta_collected() ->void:
	#print("Fruta coleccionada")
	frutas_obtenidas += 1
	if frutas_obtenidas == variedades_frutas:
		emit_signal("sin_frutas")