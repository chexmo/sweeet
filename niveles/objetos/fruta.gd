extends Node2D

export var nro = 0
signal fruta_collected

func _ready() -> void:
	match (nro):
		1:
			$Sprite.texture = load("res://niveles/objetos/imagenes/Apple.png")
		2:
			$Sprite.texture = load("res://niveles/objetos/imagenes/Bananas.png")
		3:
			$Sprite.texture = load("res://niveles/objetos/imagenes/Cherries.png")
		4:
			$Sprite.texture = load("res://niveles/objetos/imagenes/Kiwi.png")
		5:
			$Sprite.texture = load("res://niveles/objetos/imagenes/Melon.png")
		6:
			$Sprite.texture = load("res://niveles/objetos/imagenes/Orange.png")
		7:
			$Sprite.texture = load("res://niveles/objetos/imagenes/Pineapple.png")
		8:
			$Sprite.texture = load("res://niveles/objetos/imagenes/Strawberry.png")


func _on_Fruta_body_entered(body: PhysicsBody2D) -> void:
	if body.is_in_group("player"):
		$AnimationPlayer.play("collected")
		emit_signal("fruta_collected")



func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if anim_name == "collected" :
		self.queue_free()
