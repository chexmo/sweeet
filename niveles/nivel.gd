extends Node2D

func _on_HUD_tiempofuera() -> void:
	get_tree().change_scene("res://otros/mensajes/perdiste.tscn")

func _on_Frutas_sin_frutas() -> void:
	get_tree().change_scene("res://otros/mensajes/ganaste.tscn")

func _on_HUD_btmenu_presionado() -> void:
	get_tree().change_scene("res://menu/main.tscn")

func _on_HUD_empezar() -> void:
	$Personaje._habilitar_movimiento()
	print("EMPEZAMOSSS")
