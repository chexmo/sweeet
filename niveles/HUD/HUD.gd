extends Control

export var max_time = 120.0
export var help_delay = 7.0
export var cuenta_reg_delay = 1.2
var counter = 0.0
var time = 0.0
var jugando = false
var estado = 3
signal tiempofuera
signal btmenu_presionado
signal empezar

func _ready() -> void:
	$Time_left.wait_time = max_time
	$Instrucciones/Timer.wait_time = help_delay

func _process(delta: float) -> void:
	counter+=delta
	if counter>=0.5 :
		time = $Time_left.time_left
	#	print("A " + str(time)) #para debuggear
		time = floor((time / max_time) * 100)
	#	print("B " + str(time)) #para debuggear
		if time>0:
			$ProgressBar.value = time
		else:
			if jugando:
				emit_signal("tiempofuera")
		counter=0.0

func _on_Timer_timeout() -> void:
#	self.emit_signal("desaparecen_instrucciones")
	$Instrucciones.visible = false
	$Carteles/Counter.start(0.1)


func _on_Button_button_down() -> void:
	emit_signal("btmenu_presionado")

func _on_Counter_timeout() -> void:
	estado = estado - 1
	print("estado"+ str(estado))
	if estado == 2:
		$Carteles/Label3.text = "PREPARADO"
		$Carteles/Counter.start(cuenta_reg_delay)
		
	if estado == 1:
		$Carteles/Label3.text = "LISTO"
#		$Carteles/Counter.start(cuenta_reg_delay)
		
	if estado == 0:
		$Carteles/Label3.text = "YAAAA"
#		$Carteles/Counter.start(cuenta_reg_delay)
		
	if estado < 0:
		$Carteles/Counter.stop()
		$Carteles/Label3.visible = false
		jugando = true
		$Time_left.start(max_time)
		emit_signal("empezar")
		
	print($Carteles/Counter.is_stopped())